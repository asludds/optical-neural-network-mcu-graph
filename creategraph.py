import matplotlib.pyplot as plt
import scipy as sp
import numpy as np
import csv

def convert_model_name_to_computations(model_name):
    model_name = model_name.split("/")[1]
    model_split = model_name.split("_")
    row = int(model_split[0])
    col = int(model_split[1].split('.')[0])
    second_hidden = int(model_split[3].split('.')[0])
    hidden = int(model_split[2])

    input_layer = row*col
    if(second_hidden != 0 and hidden != 0):
        MCU = input_layer * hidden + hidden * second_hidden + second_hidden * 10
    elif(second_hidden == 0 and hidden != 0):
        MCU = input_layer * hidden + hidden * 1000
    elif(second_hidden == 0 and hidden == 0):
        MCU = input_layer * 10
    return MCU

def readable_model_name(model_name):
    model_name = model_name.split('/')[1]
    model_split = model_name.split("_")
    row = int(model_split[0])
    col = int(model_split[1])
    interpolation = model_split[2]
    first_layer_activation = model_split[3]
    hidden = int(model_split[4].split('.')[0])
    input_layer = row*col
    return input_layer, hidden, interpolation, first_layer_activation



def create_plot():
    fig, ax = plt.subplots()
    plot = ax.scatter([], [])
    ax.set_xlim(1e2, 1e7)
    ax.set_ylim(0, 15)
    ax.set_xscale('log')
    ax.set_title('Plot of models MCUs vs Error Rate')
    ax.set_xlabel("MCUs")
    ax.set_ylabel("Error rate of model")
    accuracy_dict = {}

    with open('accuracy.txt') as f:
        for line in f:
            modelname, accuracy = line.split(" ")
            accuracy_dict.update({modelname : float(accuracy)})

    count = 1
    for key,value  in accuracy_dict.items():
        error_rate = 100*(1-value)
        MCUs = convert_model_name_to_computations(key)
        ax.plot(MCUs, error_rate, "or")
        ax.annotate(count, xy=(MCUs+5,error_rate+0.1))
        # input_layer, hidden, interpolation, first_layer_activation = readable_model_name(key)
        count += 1

    # with open("model_data.csv",'w',newline='') as csvfile:
        # spamwriter = csv.writer(csvfile, delimiter=',',
                            # quotechar='|', quoting=csv.QUOTE_MINIMAL)
        # spamwriter.writerow(["Model Number","Input Layer Size","Hidden Layer Size","Interpolation","First Layer Activation Function","Accuracy"])
        # 
        # count = 1
        # for key,value  in accuracy_dict.items():
            # input_layer, hidden, interpolation, first_layer_activation = readable_model_name(key)
            # spamwriter.writerow([count,input_layer , hidden , interpolation, first_layer_activation,value])
            # 
            # 
            # count += 1

    plt.show()

if __name__ == "__main__":
    create_plot()
