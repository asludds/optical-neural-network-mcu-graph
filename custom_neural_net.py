
'''Trains a simple convnet on the MNIST dataset.
Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation, AveragePooling2D
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from scipy.misc import imread, imresize
from keras.models import model_from_json, load_model
import scipy as sp
import numpy as np
import random
import os
from progress.bar import Bar
import tensorflow as tf

"This class will allow for trained neural networks to be run on images that are sent optically"
class neural_network():
    
    def __init__(self,image,model_file):
        self.image = image
        self.model_file = model_file
        self.model_folder = 'nn_model/'
    
    def run_model_on_image(self,new_weights=False):
        x = self.image
        print(self.image)
        #compute a bit-wise inversion so black becomes white and vice versa
        # x = np.invert(x)
        #make it the right size
        x = sp.misc.imresize(x,(28,28))
        #convert to a 4D tensor to feed into our model
        x = x.reshape((1,28*28))
        x = x.astype('float32')
        x /= 255

        model = load_model(self.model_folder + self.model_file + '.h5')
        if(new_weights != False):
            model.set_weights(new_weights)
        out = model.predict(x)
        print(np.argmax(out))
        return np.argmax(out)

    def load_weights_from_model(self):
        model = load_model(self.model_folder + self.model_file + '.h5')
        weights = model.get_weights()
        return weights

    def load_weights_from_model_8bit(self):
        model = load_model(self.model_folder + self.model_file + '.h5')
        weights = model.get_weights()
        weightlist8bit = []
        for i in weights:
            weightlist8bit.append(i.astype("float16"))
        return weightlist8bit

        
    def batch_predict(self,to_predict,new_weights=False):
        model = load_model(self.model_folder + self.model_file + '.h5')
        if(new_weights != False):
            model.set_weights(new_weights)
        correct_count = 0
        bar = Bar("Testing MNIST on ONN",max=len(to_predict))
        for index,i in enumerate(to_predict):
            mnist_image , numberOfImage = load_mnist_image(number=i)

            # x = sp.misc.imresize(mnist_image,(28,28))
            x = sp.misc.imresize(mnist_image,(14,14))


            # x = x.reshape((1,28*28))
            x = x.reshape((1,14*14))
            x = x.astype('float32')
            x /= 255

            out = model.predict(x)
            prediction = np.argmax(out)
            if(prediction == i):
                correct_count += 1
            bar.next()
        bar.finish()
        return correct_count

    def train_model(self,res,model_directory,first,second,num_neurons_first,num_neurons_second):
        batch_size = 128
        num_classes = 10
        epochs = 30

        print(self.image.shape)

        # input image dimensions
        img_rows, img_cols = res[0],res[1]

        ((train_img, train_num), (test_img, test_num)) = tf.keras.datasets.mnist.load_data()

        # the data, shuffled and split between train and test sets
        # (x_train, y_train), (x_test, y_test) = mnist.load_data()

        def resize_xs(xs):
            N,dim = xs.shape
            if dim != 28*28:
                print('size wrong', dim)
                return
            xs_new = np.zeros((N, img_rows*img_cols),dtype='float32')
            for i in range(N):
                xi = xs[i].reshape(28,28)
                temp = sp.misc.imresize(xi,size=(img_rows,img_cols))
                # xs_new[i] = xi[::2,::2].reshape(14*14)
                xs_new[i] = temp.reshape(img_rows * img_cols)
            return xs_new

        train_img = train_img.reshape([60000, 28, 28, 1]) / 255.; 
        test_img = test_img.reshape([10000, 28, 28, 1]) / 255.
        train_lbl = (np.outer(np.arange(10), np.ones([60000])) 
             == np.outer([1], train_num)).astype(int).transpose()
        test_lbl = (np.outer(np.arange(10), np.ones([10000])) 
             == np.outer([1], test_num)).astype(int).transpose()


        model = Sequential()
        if(res[0]==28):
            pass
        elif(img_rows == 14):
            model.add(AveragePooling2D(pool_size=2, padding='valid',data_format='channels_last'))

        elif(img_rows==7):
            model.add(AveragePooling2D(pool_size=4, padding='valid',data_format='channels_last'))

        model.add(Flatten(input_shape=(img_rows, img_cols, 1)))
        if(num_neurons_first != 0):
            model.add(Dense(num_neurons_first, activation=first, input_shape=(img_rows*img_cols,)))
        if(num_neurons_second != 0):
            model.add(Dense(num_neurons_second, activation=second, input_shape=(img_rows*img_cols,)))
        model.add(Dense(num_classes, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer='rmsprop',
                    metrics=['accuracy'])
        model.fit(train_img, train_lbl, validation_split=0.2, epochs=20)
        score = model.evaluate(test_img, test_lbl)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
        print("Saving ",model_directory)
        model.save(model_directory)


        with open("accuracy.txt", "a") as myfile:
            myfile.write(model_directory + " " + str(score[1]) + "\n")



def load_mnist_image(number=9):
    choice = random.choice(os.listdir("mnist_images/training/" + str(number)))
    image = sp.misc.imread("mnist_images/training/" + str(number) + "/" + choice , mode='L')
    return image,number




if __name__ == "__main__":
    # pass
    image,number = load_mnist_image(number=6)
    neuralNetwork = neural_network(image=image,model_file="model")
    # neuralNetwork.train_model()
    # to_predict = [int(i/(1000/10)) for i in range(1000)]
    # weights = neuralNetwork.load_weights_from_model_8bit()
    # correct_count = neuralNetwork.batch_predict(to_predict=to_predict,new_weights=weights)
    # print("Accuracy: ", correct_count/1000 * 100, "%")
    dicty = [{"res":(28,28),"first":800,"second":0},
             {"res":(28,28),"first":256,"second":256},
             {"res":(14,14),"first":64,"second":32},
             {"res":(14,14),"first":32,"second":16},
             {"res":(7,7),"first":32,"second":16},
             {"res":(7,7),"first":16,"second":16},
             {"res":(7,7),"first":0,"second":0}]

    model_directory = ""
    for elem in dicty:
        print(elem)
        model_directory = "models/" + str(elem['res'][0]) + "_" + str(elem['res'][1]) + "_" + str(elem['first']) + "_" + str(elem['second']) + ".h5"
        neuralNetwork.train_model(res=elem["res"],model_directory= model_directory,first="relu",second="relu",num_neurons_first=elem['first'],num_neurons_second=elem['second'])


